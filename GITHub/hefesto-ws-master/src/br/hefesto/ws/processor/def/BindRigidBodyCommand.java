package br.hefesto.ws.processor.def;

import org.json.JSONObject;

import br.hefesto.simulation.HRigidBody;
import br.hefesto.simulation.PhysicSimulation;
import br.law123.core.Matrix3;
import br.law123.core.Quaternion;
import br.law123.core.Vector3;
import br.law123.rigidbody.RigidBody;

public class BindRigidBodyCommand implements CommandProcessor {

    @Override
    public JSONObject process(PhysicSimulation simulation, JSONObject data) {
    	try {
			Thread.sleep(5);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
        HRigidBody hbody = new HRigidBody(data.getString("_id"));
        hbody.setBody(new RigidBody());

        hbody.getBody().setPosition(getVector3(data.getJSONObject("_position")));
        hbody.getBody().setOrientation(getQuaternion(data.getJSONObject("_orientation")));
        hbody.getBody().setVelocity(getVector3(data.getJSONObject("_velocity")));
        hbody.getBody().setAcceleration(getVector3(data.getJSONObject("_aceleration")));
        hbody.getBody().setRotation(getVector3(data.getJSONObject("_rotation")));

        hbody.getBody().setInertiaTensor(getMatrix3(data.getJSONObject("_inertiaTensor").getJSONObject("elements")));
        hbody.getBody().setLinearDamping(data.getDouble("_linearDamping"));
        hbody.getBody().setAngularDamping(data.getDouble("_angularDamping"));

        hbody.getBody().setCanSleep(data.getBoolean("_canSleep"));
        
        hbody.getBody().setAwake();

        boolean added = simulation.addRigidBody(hbody);

        JSONObject result = new JSONObject();
        result.put("added", added);

        return result;
    }

    private Vector3 getVector3(JSONObject obj) {
        return new Vector3(obj.getDouble("x"), obj.getDouble("y"), obj.getDouble("z"));
    }

    private Quaternion getQuaternion(JSONObject obj) {
        return new Quaternion(obj.getDouble("_w"), obj.getDouble("_x"), obj.getDouble("_y"), obj.getDouble("_z"));
    }

    private Matrix3 getMatrix3(JSONObject obj) {
        return new Matrix3(
        //
        obj.getDouble("0"), obj.getDouble("1"), obj.getDouble("2"), //
        obj.getDouble("3"), obj.getDouble("4"), obj.getDouble("5"), //
        obj.getDouble("6"), obj.getDouble("7"), obj.getDouble("8"));
    }

}

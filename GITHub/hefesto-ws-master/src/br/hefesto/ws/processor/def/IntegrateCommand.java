package br.hefesto.ws.processor.def;

import java.util.Map;
import java.util.Map.Entry;

import org.json.JSONArray;
import org.json.JSONObject;

import br.hefesto.simulation.HRigidBody;
import br.hefesto.simulation.PhysicSimulation;
import br.law123.core.Quaternion;
import br.law123.core.Vector3;

public class IntegrateCommand implements CommandProcessor {

    @Override
    public JSONObject process(PhysicSimulation simulation, JSONObject data) {
        double duration = data.getDouble("duration");
        simulation.integrate(duration);

        Map<String, HRigidBody> bodys = simulation.getRigidBodys();

        JSONArray array = new JSONArray();

        for (Entry<String, HRigidBody> e : bodys.entrySet()) {
            JSONObject obj = new JSONObject();
            obj.put("id", e.getKey());
            
            obj.put("_position", getJSONVector3(e.getValue().getBody().getPosition()));
            
            //obj.put("_orientation", getJSONQuaternion(e.getValue().getBody().getOrientation()));

            float[] mat = new float[16];
            e.getValue().getBody().getGLTransform(mat);
            obj.put("_transform", mat);

            array.put(obj);
        }

        JSONObject _rigidBodys = new JSONObject();
        _rigidBodys.put("_rigidBodys", array);

        return _rigidBodys;
    }

	private JSONObject getJSONVector3(Vector3 v) {
		JSONObject data = new JSONObject();
		data.put("x", v.getX());
		data.put("y", v.getY());
		data.put("z", v.getZ());
		return data;
	}
	
	private JSONObject getJSONQuaternion(Quaternion q) {
		JSONObject data = new JSONObject();
		data.put("_w", q.getR());
		data.put("_x", q.getI());
		data.put("_y", q.getJ());
		data.put("_z", q.getK());
		
		return data;
	}

}

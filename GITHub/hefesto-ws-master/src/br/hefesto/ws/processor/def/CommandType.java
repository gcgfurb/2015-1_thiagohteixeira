package br.hefesto.ws.processor.def;

public enum CommandType {

	/** Gera um novo corpo rigido */
    NEW_SIMULATION("NEW_SIMULATION", NewSimulatinoCommand.class),
    /** Gera um novo corpo rigido */
    BIND_RIGID_BODY("BIND_RIGID_BODY", BindRigidBodyCommand.class),
    /** Gera uma nova particula */
    BIND_PARTICLE("BIND_PARTICLE", BindParticleCommand.class),
    /** Gera uma nova collisao */
    BIND_COLLISION("BIND_COLLISION", BindCollisionCommand.class),
    /** Integrate */
    INTEGRATE("INTEGRATE", IntegrateCommand.class);

    private String command;
    private Class<? extends CommandProcessor> processor;

    private CommandType(String command, Class<? extends CommandProcessor> processor) {
        this.command = command;
        this.processor = processor;
    }

    public String getCommand() {
        return command;
    }

    public Class<? extends CommandProcessor> getProcessor() {
        return processor;
    }

    public CommandType valueOfString(String cmd) {
        for (CommandType c : values()) {
            if (c.getCommand().equals(cmd)) {
                return c;
            }
        }
        return null;
    }

}

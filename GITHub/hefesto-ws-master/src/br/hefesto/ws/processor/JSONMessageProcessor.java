package br.hefesto.ws.processor;

import org.json.JSONObject;

import br.hefesto.simulation.PhysicSimulation;
import br.hefesto.ws.processor.def.CommandProcessor;
import br.hefesto.ws.processor.def.CommandType;

public class JSONMessageProcessor {
    
    public static final JSONObject process(PhysicSimulation simulation, CommandType cmd, JSONObject data) throws Exception {
        CommandProcessor processor = cmd.getProcessor().newInstance();

		return processor.process(simulation, data.getJSONObject("data"));
    }

}

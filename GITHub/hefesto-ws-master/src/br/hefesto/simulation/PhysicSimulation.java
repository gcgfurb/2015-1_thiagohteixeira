package br.hefesto.simulation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import br.law123.collide.CollisionBox;
import br.law123.collide.CollisionData;
import br.law123.collide.CollisionDetector;
import br.law123.collide.CollisionPlane;
import br.law123.core.Vector3;
import br.law123.rigidbody.contact.Contact;
import br.law123.rigidbody.contact.ContactResolver;

public class PhysicSimulation {

    private final long id;

    protected static final int maxContacts = 256;

    private final Map<String, HRigidBody> rigidBodys = new HashMap<String, HRigidBody>();
    private final List<Collision> collisions = new ArrayList<Collision>();

    private Contact[] contacts = new Contact[maxContacts];
    private CollisionData cData = new CollisionData();
    private ContactResolver resolver = new ContactResolver(maxContacts * 8);

    public PhysicSimulation(long id) {
        this.id = id;
        cData.setContactArray(contacts);

        // Set up the collision data structure
        cData.reset(maxContacts);
        cData.setFriction(6.9);
        cData.setRestitution(0.6);
        cData.setTolerance(0.1);
    }

    public long getId() {
        return id;
    }

    public Map<String, HRigidBody> getRigidBodys() {
        return rigidBodys;
    }

	public boolean addRigidBody(HRigidBody body) {
		if (rigidBodys.containsKey(body.getId())) {
			return false;
		}
		rigidBodys.put(body.getId(), body);
		return true;
	}

    public void integrate(double duration) {

        // Update the objects
        integrateObjects(duration);

        // Perform the contact generation
        generateContacts();

        // Resolve detected contacts
        resolver.resolveContacts(cData.getContactArray(), cData.getContactCount(), duration);

    }

    protected void integrateObjects(double duration) {
        for (HRigidBody rb : rigidBodys.values()) {
            rb.getBody().integrate(duration);
            rb.calculateInternals();
        }
    }

    protected void generateContacts() {
        // Create the ground plane data
        CollisionPlane plane = new CollisionPlane();
        plane.setDirection(new Vector3(0, 1, 0));
        plane.setOffset(0);
        
        
        // Set up the collision data structure
        
        cData.reset(maxContacts);
        cData.setFriction(0.1);
        cData.setRestitution(0.1);
        cData.setTolerance(0.1);
        
        for(HRigidBody b : rigidBodys.values()) {
        	CollisionBox box = new CollisionBox();
        	box.setBody(b.getBody());
        	box.setHalfSize(new Vector3(5, 5, 5));
        	box.calculateInternals();
        	CollisionDetector.boxAndHalfSpace(box, plane, cData);
        }


        for (Collision col : collisions) {
            switch (col.getType()) {
                case BOX_AND_BOX:
                    break;
                case BOX_AND_HALFSPACE:
                    //CollisionDetector.boxAndHalfSpace(col.getRb1(), col.getRb2(), cData);
                    break;
                case BOX_AND_POINT:
                    break;
                case BOX_AND_SPHERE:
                    break;
                case SPHERE_AND_HALFSPACE:
                    break;
                case SPHERE_AND_SPHERE:
                    break;
                case SPHERE_AND_TRUEPLANE:
                    break;

                default:
                    System.err.println("Unkown collision type: " + col.getType());
            }
        }

    }
    
}

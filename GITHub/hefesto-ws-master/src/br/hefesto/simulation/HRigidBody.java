package br.hefesto.simulation;

import br.law123.collide.CollisionPrimitive;

/**
 * Representa��o um corpo r�gido que pode colidir.
 * 
 * @author teixeira
 */
public class HRigidBody extends CollisionPrimitive {

	private final String id;

	public HRigidBody(String id) {
		this.id = id;
	}

	public String getId() {
		return id;
	}

}

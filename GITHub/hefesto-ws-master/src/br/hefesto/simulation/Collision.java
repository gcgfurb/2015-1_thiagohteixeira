package br.hefesto.simulation;


/**
 * Representa��o de uma poss�vel colis�o.
 * 
 * @author teixeira
 */
public class Collision {

	private final CollisionType type;
	private final HRigidBody rb1;
	private final HRigidBody rb2;

	public Collision(CollisionType type, HRigidBody rb1, HRigidBody rb2) {
		this.type = type;
		this.rb1 = rb1;
		this.rb2 = rb2;
	}

	public CollisionType getType() {
		return type;
	}

	public HRigidBody getRb1() {
		return rb1;
	}

	public HRigidBody getRb2() {
		return rb2;
	}

}

HEFESTO.RigidBody = function (mesh) {


	this._id = HEFESTO.guid();
	this._mesh = mesh;
	this._binded = false;

	this._position = mesh.position;
	this._orientation = new THREE.Quaternion();
	this._velocity = new THREE.Vector3();
	this._aceleration = new THREE.Vector3();
	this._rotation = new THREE.Vector3();

	this._inverseMass = 0.0;
	this._inertiaTensor = new THREE.Matrix3();
	this._linearDamping = 0.0;
	this._angularDamping = 0.0;
	this._canSleep = true;

	this._matrix = new THREE.Matrix4();

}

HEFESTO.RigidBody.prototype = {
	construtor: HEFESTO.RigidBody,

	get position () {
		return this._position;
	},

	set position (value) {
		this._position = value;
	},

	get orientation () {
		return this._orientation;
	},

	set orientation (value) {
		this._orientation = value;
	},

	get velocity () {
		return this._velocity;
	},

	set velocity (value) {
		this._velocity = value;
	},

	get aceleration () {
		return this._aceleration;
	},

	set aceleration (value) {
		this._aceleration = value;
	},

	get rotation () {
		return this._rotation;
	},

	set rotation (value) {
		this._rotation = value;
	},

	get inverseMass () {
		return this._inverseMass;
	},

	set inverseMass (value) {
		this._inverseMass = value;
	},

	get inertiaTensor () {
		return this._inertiaTensor;
	},

	set inertiaTensor (value) {
		this._inertiaTensor = value;
	},

	get linearDamping () {
		return this._linearDamping;
	},

	set linearDamping (value) {
		this._linearDamping = value;
	},

	get angularDamping () {
		return this._angularDamping;
	},

	set angularDamping (value) {
		this._angularDamping = value;
	},

	get canSleep () {
		return this._canSleep;
	},

	set canSleep (value) {
		this._canSleep = value;
	},

	get matrix () {
		return this._matrix;
	},

	set matrix (value) {
		this._matrix = value;
	}

};
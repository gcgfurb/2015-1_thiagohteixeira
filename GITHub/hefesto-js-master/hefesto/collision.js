HEFESTO.CollisionType = {
	SPHERE_AND_HALFSPACE: "SPHERE_AND_HALFSPACE",
	SPHERE_AND_TRUEPLANE: "SPHERE_AND_TRUEPLANE",
	SPHERE_AND_SPHERE: "SPHERE_AND_SPHERE",
	BOX_AND_HALFSPACE: "BOX_AND_HALFSPACE",
	BOX_AND_BOX: "BOX_AND_BOX",
	BOX_AND_POINT: "BOX_AND_POINT",
	BOX_AND_SPHERE: "BOX_AND_SPHERE"
}
HEFESTO.CollisionData = function(friction, restitution, tolerance) {
	this._id = HEFESTO.guid();
	this._friction = friction;
	this._restitution = restitution;
	this._tolerance = tolerance;
};

HEFESTO.CollisionData.prototype = {
	construtor: HEFESTO.CollisionData,

	get friction () {
		return this._friction;
	},

	set friction (value) {
		this._friction = value;
	},

	get restitution () {
		return this._restitution;
	},

	set restitution (value) {
		this._restitution = value;
	},

	get tolerance () {
		return this._tolerance;
	},

	set tolerance (value) {
		this._tolerance = value;
	};
};

HEFESTO.Collision = function(type, data, body1, body2) {
	this._id = HEFESTO.guid();
	this._type = type;
	this._data = data;
	this._body1 = body1.id;
	this._body2 = body2.id;
};

HEFESTO.Collision.prototype = {
	construtor: HEFESTO.Collision,

	get type () {
		return this._type;
	},

	set type (value) {
		this._type = value;
	},

	get data () {
		return this._data;
	},

	set data (value) {
		this._data = value;
	},

	get body1 () {
		return this._body1;
	},

	set body1 (value) {
		this._body1 = value;
	},

	get body2 () {
		return this._body2;
	},

	set body2 (value) {
		this._body2 = value;
	}
};
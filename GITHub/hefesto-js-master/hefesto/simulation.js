/** 

 * Tipos de comandos para o servidor.
 */
HEFESTO.CommandType = {
	/** Gera um novo corpo rigido */
	NEW_SIMULATION: "NEW_SIMULATION",
    /** Gera um novo corpo rigido */
    BIND_RIGID_BODY: "BIND_RIGID_BODY",
    /** Gera uma nova particula */
    BIND_PARTICLE: "BIND_PARTICLE",
    /** Gera uma nova collisao */
    BIND_COLLISION: "BIND_COLLISION",
    /** Gera novos dados de collisao */
    BIND_COLLISION_DATA: "BIND_COLLISION_DATA",
    /** Integrate */
    INTEGRATE: "INTEGRATE"
};

/**
 * Objeto de simulação de física newtoniana em 3D.
 */
HEFESTO.Simulation = function () {

	this._simulationId = -1;
	this._active = false;
	this._simulationWS = new HEFESTO.SimulationWS(this);

	// array de corpos rígidos
	this._rigidBodys = [];
	// array de partículas
	this.particles = [];
	// array de colisões
	this._collisions = [];
	// array de dados de colisão
	this._collisionDatas = [];
};

/**
 * Definições do objeto de simulação.
 */
HEFESTO.Simulation.prototype = {
	constructor : HEFESTO.Simulation,


	/**
	 * Faz o 'bind' do corpo rídido no servidor.
	 */
	init: function () {
		this._simulationWS.init();
	},

	/**
	 * Faz o 'bind' do corpo rídido no servidor.
	 */
	bindRigidBody: function (body) {
		var mesh = body._mesh;
		body._mesh = undefined;
		try {
			this._simulationWS.sendMessage(HEFESTO.CommandType.BIND_RIGID_BODY, body);
		} finally {
			body._mesh = mesh;
		}

		this._rigidBodys[body._id] = body;	
	},

	/**
	 * Faz o 'bind' da partícula no servidor.
	 */
	bindParticle: function (particle) {
		this._simulationWS.sendMessage(HEFESTO.CommandType.BIND_PARTICLE, particle);

		this._particles[particle._id] = particle;
	},

	/**
	 * Faz o 'bind' da colisão no servidor.
	 */
	bindCollision: function (collision) {
		this._simulationWS.sendMessage(HEFESTO.CommandType.BIND_COLLISION, collision);
		
		this._collisions[collision._id] = collision;
	},

	/**
	 * Faz o 'bind' dos dados de colisão no servidor.
	 */
	bindCollision: function (data) {
		this._simulationWS.sendMessage(HEFESTO.CommandType.BIND_COLLISION_DATA, data);
		
		this._collisions[data._id] = data;
	},

	removeRigidBody:  function (body) {

	},

	removeParticle:  function (particle) {

	},

	removeCollision:  function (collision) {

	},

	/**
	 * Faz a integração com os objetos físicos.
	 */
	integrate: function (duration) {
		var data = {
			'duration' : duration
		};
		this._simulationWS.sendMessage(HEFESTO.CommandType.INTEGRATE, data);
	},

	/**
	 * Verifica se está aguardando alguma mensagem ser processada.
	 */
	isBusy: function () {
		return !this._active | (this._active & this._simulationWS.isWaitingMessage());
	},

	/**
	 * Faz o 'bind' da partícula no servidor.
	 */
	onmessage: function (type, data) {
		if (type == HEFESTO.CommandType.NEW_SIMULATION) {
			this._simulationId = data.id;
			this._active = true;
		} else if (type == HEFESTO.CommandType.BIND_RIGID_BODY) {
			log('RigidBody successfully added.');
		} else if (type == HEFESTO.CommandType.BIND_PARTICLE) {
			log('Particle successfully added.');
		} else if (type == HEFESTO.CommandType.BIND_COLLISION) {
			log('Collision successfully added.');
		} else if (type == HEFESTO.CommandType.BIND_COLLISION_DATA) {
			log('CollisionData successfully added.');
		} else if (type == HEFESTO.CommandType.INTEGRATE) {
			this.onintegrate(data);
		} else {
			log('Unknow message type: ' + type);
		}
	},

	/**
	 * Processa o resultado da integração.
	 */
	onintegrate: function (data) {
		
		for (i = 0; i < data._rigidBodys.length; i++) { 
		    var _rb = data._rigidBodys[i];

		    var rb = this._rigidBodys[_rb.id];

		    //rb._mesh.position.x =  _rb._position.x;
		    //rb._mesh.position.y =  _rb._position.y;
		    //rb._mesh.position.z =  _rb._position.z;
		    var p = new THREE.Vector3();
		    p.x =  _rb._position.x;
		    p.y =  _rb._position.y;
		    p.z =  _rb._position.z;
		    

		    //rb._mesh.matrix = this._transformToMatrix4(_rb._transform);
		    var a = _rb._transform;
		    var m = new THREE.Matrix4();
		    m.set(a[0], a[1], a[2], a[3], a[4], a[5], a[6], a[7], a[8], a[9], a[10], a[11], a[12], a[13], a[14], a[15]);

		    //tentar setar o orientacao e posicao

		    rb._mesh.matrix.identity();
		    rb._mesh.applyMatrix(m);

		    rb._mesh.updateMatrix();
		    rb._mesh.matrix.setPosition(p);
			//rb._mesh.matrixWorldNeedsUpdate = true;
		    //rb._mesh.updateMatrixWorld(true);
		}

		log('Integrate simulation: ' + this._simulationId);
	}
};

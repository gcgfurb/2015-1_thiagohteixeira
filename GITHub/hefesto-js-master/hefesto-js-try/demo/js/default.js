var scene = new THREE.Scene();
var createDefaultCamera = function() {
	/*var camera = new THREE.PerspectiveCamera( 800, window.innerWidth / window.innerHeight, 1, 1000 );

	camera.position.y = 200; 
	camera.position.x = 150; 
	camera.position.z = 500;

	camera.lookAt(new THREE.Vector3( 0, 0, 0 ));
*/
	var camera = new THREE.PerspectiveCamera(55.0, window.innerWidth / window.innerHeight, 0.5, 3000000);
	camera.position.set(50, 300, -500);

	return camera;
};

var camera = createDefaultCamera();


container = document.createElement( 'div' );
document.body.appendChild( container );

var renderer = new THREE.WebGLRenderer();
renderer.setSize(window.innerWidth, window.innerHeight);
renderer.setClearColor( 0xffffff, 1);
document.body.appendChild(renderer.domElement);

container.appendChild( renderer.domElement );

//scene.add(new THREE.AxisHelper(300));
//scene.add(new THREE.GridHelper(300, 15));


stats = new Stats();
stats.domElement.style.position = 'absolute';
stats.domElement.style.top = '0px';
container.appendChild( stats.domElement );

var simulation = new HEFESTO.Simulation();
simulation.init();

var timming = new HEFESTO.Timming();

// Initialize Orbit control		
controls = new THREE.OrbitControls(camera, renderer.domElement);

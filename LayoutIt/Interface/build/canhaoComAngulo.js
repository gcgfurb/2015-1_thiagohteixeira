window.onload = init;

var camera;
var render;
var scene;
var div;
var canhao;
var canhao2;

var simulation;
var timming;
var cdGround;
cdGround = new HEFESTO.CollisionData(0.9, 0.9, 0.1);
cdGround.id = 'cdGround';
cdGround.maxCollision = 256 * 256;

function init() {
	simulation = new HEFESTO.Simulation();
	timming = new HEFESTO.Timming();
	simulation.init();
	continueInitilization();

}
function continueInitilization() {
	if (!simulation.isBusy()) {

		simulation.bindCollisionData(cdGround);
		carregaCena();
		timming.start();
		timming.update();
		renderiza();
	} else {
		requestAnimationFrame(continueInitilization);
	}
}

function carregaCena() {
	camera = new THREE.PerspectiveCamera(75, window.innerWidth
			/ window.innerHeight, 0.1, 1000);

	camera.position.set(0.03620912515119242, 7.4781311558765085,
			-142.26660252588118);
	camera.lookAt(0, 0, 0);

	// movimenta a camera
	controls = new THREE.TrackballControls(camera);
	controls.rotateSpeed = 1.0;
	controls.zoomSpeed = 1.2;
	controls.panSpeed = 0.8;
	controls.noZoom = false;
	controls.noPan = false;
	controls.staticMoving = true;
	controls.dynamicDampingFactor = 0.3;

	scene = new THREE.Scene();

	div = document.getElementById('canhao');
	div.innerHTML = "";

	render = new THREE.WebGLRenderer();
	render.setSize(div.offsetWidth, div.offsetHeight);
	render.setClearColor(0xFFFFFF);
	div.appendChild(render.domElement);

	var baseGeometry = new THREE.BoxGeometry(300, 1, 300);
	var baseMaterial = new THREE.MeshBasicMaterial({
		color : 0x00FF00,
		transparent : true,
		opacity : 0.2
	});
	var base = new THREE.Mesh(baseGeometry, baseMaterial);

	scene.add(base);

	var directionalLight = new THREE.DirectionalLight(0xffffff, 20);
	directionalLight.position.set(0, 20, 0);
	scene.add(directionalLight);
	scene.add(new THREE.AxisHelper(300));
	scene.add(new THREE.GridHelper(150, 15));
	
	canhao = new THREE.PerspectiveCamera(9, 1, 1, 90);
	canhao.position.set(0, 10, 0);
	//canhao.rotation.y = -1.620000000000001;
	// canhao.lookAt(0,0,0);
	scene.add(canhao);
	var helperCanhao = new THREE.CameraHelper(canhao, 10);
	scene.add(helperCanhao);

	var num = '0';
	
	
	canhao2 = new Canhao(scene, 100, parseFloat(num), 0);
	canhao2.setAnguloXY(parseFloat(num));
	
	console.log(canhao2.anguloXY);
	
	criaEsfera(5);
	window.addEventListener('keydown', whatKey);
}

function whatKey(event) {
	switch (event.keyCode) {
	// Esquerda
	case 65:
		// console.log(cannon.angleZ);
		canhao.rotation.y -= 0.02;
		console.log(canhao.rotation.y);
		canhao2.anguloZ -=2;
		break;
	// Direita
	case 68:
		// console.log(cannon.angleZ);
		canhao.rotation.y += 0.02;
		console.log(canhao.rotation.y);
		canhao2.anguloZ +=2;
		break;
	// Baixo
	case 83:
		// console.log(cannon.angle);
		canhao2.setAnguloXY(canhao2.getAnguloXY()-2);
		console.log(canhao2.anguloXY);
		canhao.rotation.x -= 0.02;
		break;
	// Cima
	case 87:
		// console.log(cannon.angle);
		canhao2.setAnguloXY(canhao2.getAnguloXY()+2);
		console.log(canhao2.getAnguloXY());
		canhao.rotation.x += 0.02;
		break;
	case 32:
		criaEsfera(5);
		// createCannonBall(cannon, 0x9f9f9f, cdMetal);
		break;
	}
	console.log("Funcionou!!!" + event.keyCode);
}

function renderiza() {
	requestAnimationFrame(renderiza);
	if (!simulation.isBusy()) {
		simulation.integrate(timming.getLastFrameDuration() * 0.001);

		controls.update();
		timming.update();
		render.render(scene, camera);
	}
}

function criaEsfera(tamanho) {
	var geometria = new THREE.SphereGeometry(tamanho,32,32);
	var material = new THREE.MeshBasicMaterial({color: 0xFF0000 * Math.random()});
	
	var mesh = new THREE.Mesh(geometria,material);
	mesh.position.y += canhao.position.y;
	mesh.position.z -= canhao.far;
	
	scene.add(mesh);
	var velocidade = canhao2.determineVelocityVector();
	console.log(velocidade);
	bindBall(mesh,velocidade,tamanho);
}

function bindBall(mesh, valecidade, tamanho) {
	mesh.matrixAutoUpdate = false;
	var rb = new HEFESTO.RigidBody(mesh);
	//rb.bindContactData = true;
	rb.radius = tamanho;

	rb.mass = 20;
	//rb.velocity.y = 10.0;
	//rb.velocity.x = Math.random() * 800;
	rb.canSleep = false;
	rb.velocity.x = valecidade.x;
	rb.velocity.y = valecidade.y;
	rb.velocity.z = valecidade.z;

	rb.acceleration.y = -10.0;
	rb.orientation.r = 1;
	//rb.useWorldForces = true;
	rb.inertiaTensor.set(5360957.0, -0.0, -0.0, -0.0, 5360957.0, -0.0, -0.0, -0.0, 5360957.0);
	rb.linearDamping = 0.99;
	rb.angularDamping = 0.8;

	simulation.bindRigidBody(rb);
	bindColisionGround(rb)
}

function bindColisionGround(rb) {
	var collisionGround = new HEFESTO.Collision(HEFESTO.CollisionType.SPHERE_AND_TRUEPLANE, cdGround, rb, null);
	simulation.bindCollision(collisionGround);	
}

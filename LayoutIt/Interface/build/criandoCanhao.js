window.onload = start;

var camera;
var render;
var scene;
var div;
var canhao
var canhao2

// Física
var simulation;
var timming;

var cdGround;
function start() {
	simulation = new HEFESTO.Simulation();

	timming = new HEFESTO.Timming();
	simulation.init();

	cdGround = new HEFESTO.CollisionData(0.9, 0.9, 0.1);
	cdGround.id = 'cdGround';
	cdGround.maxCollision = 256 * 256;

	continueInitilization();
}

function continueInitilization() {
	if (!simulation.isBusy()) {
		timming.start();
		timming.update();
		init()
		anima();
	} else {
		requestAnimationFrame(continueInitilization);
	}
}

function init() {
	console.log(document.getElementById('canhao'));

	camera = new THREE.PerspectiveCamera(75, window.innerWidth
			/ window.innerHeight, 0.1, 1000);

	camera.position.set(0.03620912515119242, 7.4781311558765085,
			-142.26660252588118);
	camera.lookAt(0, 0, 0);

	// movimenta a camera
	controls = new THREE.TrackballControls(camera);
	controls.rotateSpeed = 1.0;
	controls.zoomSpeed = 1.2;
	controls.panSpeed = 0.8;
	controls.noZoom = false;
	controls.noPan = false;
	controls.staticMoving = true;
	controls.dynamicDampingFactor = 0.3;

	scene = new THREE.Scene();

	div = document.getElementById('canhao');

	render = new THREE.WebGLRenderer();
	render.setSize(div.offsetWidth, div.offsetHeight);

	div.appendChild(render.domElement);
	render.setClearColor(0xFFFFFF);
	canhao = new THREE.PerspectiveCamera(9, 1, 1, 90);
	canhao.position.set(0, 9, 0);
	// canhao.lookAt(0,0,0);
	scene.add(canhao);

	canhao2 = new Canhao(scene, 100, 0, 0);
	console.log(canhao2.determineVelocityVector());

	var cubo = new THREE.Mesh(new THREE.SphereGeometry(5, 32, 32),
			new THREE.MeshBasicMaterial({
				color : 0x000000
			}));
	cubo.position.z -= canhao.far;
	cubo.position.y += canhao.position.y;
	// scene.add(cubo);

	var helperCanhao = new THREE.CameraHelper(canhao, 10);
	scene.add(helperCanhao);

	render.domElement.addEventListener('mousedown', onDocumentMouseDown);

	var baseGeometry = new THREE.BoxGeometry(300, 1, 300);
	var baseMaterial = new THREE.MeshBasicMaterial({
		color : 0x00FF00,
		transparent : true,
		opacity : 0.2
	});
	var base = new THREE.Mesh(baseGeometry, baseMaterial);

	scene.add(base);

	var directionalLight = new THREE.DirectionalLight(0xffffff, 20);
	directionalLight.position.set(0, 20, 0);
	scene.add(directionalLight);

	var geometry = new THREE.SphereGeometry(20, 4, 2);
	var material = new THREE.MeshBasicMaterial({
		color : 0xff0000
	});

	scene.add(new THREE.AxisHelper(300));
	scene.add(new THREE.GridHelper(150, 15));

	var helper = new THREE.DirectionalLightHelper(directionalLight, 10)
	// console.log(helper.targetLine);
	// scene.add(helper);

	var picker = new THREE.Mesh(geometry, material);
	picker.name = 'picker';
	picker.userData.object = directionalLight;
	picker.visible = false;
	helper.add(picker);
	window.addEventListener('keydown', whatKey);

	// anima();
}

function whatKey(event) {
	switch (event.keyCode) {
	// Esquerda
	case 65:
		// console.log(cannon.angleZ);
		canhao.rotation.y -= 0.02;
		break;
	// Direita
	case 68:
		// console.log(cannon.angleZ);
		canhao.rotation.y += 0.02;
		break;
	// Baixo
	case 83:
		// console.log(cannon.angle);
		canhao.rotation.x -= 0.02;
		break;
	// Cima
	case 87:
		// console.log(cannon.angle);
		canhao.rotation.x += 0.02;
		break;
	case 32:
		criaBola();
		// createCannonBall(cannon, 0x9f9f9f, cdMetal);
		break;
	}
	console.log("Funcionou!!!" + event.keyCode);
}

function onDocumentMouseDown() {
	// console.log(camera.position);
}

function criaBola() {
	var geometryBall = new THREE.SphereGeometry(5, 32, 32);
	var material = new THREE.MeshBasicMaterial({
		color : Math.random() * 0xffffff
	});
	var mesh = new THREE.Mesh(geometryBall, material);
	// GAMU.positionMesh(mesh, position, new THREE.Vector3());
	mesh.position.z -= canhao.far;
	mesh.position.y += canhao.position.y;
	scene.add(mesh);
	mesh.matrixAutoUpdate = false;
	mesh.matrix.setPosition(new THREE.Vector3(0, canhao.position.y, -canhao.far));
	bindBall(mesh, canhao2.determineVelocityVector());
	console.log("Passou aqui!");
	// console.log(rb2.position);
}

function bindBall(mesh, velocidade) {

	var rb = new HEFESTO.RigidBody(mesh);

	rb.bindContactData = true;

	rb.radius = 5;

	rb.mass = 2;

	rb.velocity.x = velocidade.x;
	rb.velocity.y = velocidade.y;
	rb.velocity.z = velocidade.z;

	rb.acceleration.y = -10.0;

	rb.inertiaTensor.set(68.84040832519531, -0.0, -0.0, -0.0, 68.84040832519531, -0.0, -0.0, -0.0, 68.84040832519531);
	rb.linearDamping = 0.99;
	rb.angularDamping = 0.8;

	simulation.bindRigidBody(rb);				

	//balls[balls.length] = rb;

	//bindGroundCollision(rb);
	//bindCollisionWithAll(rb,cdGround);
}

function bindGroundCollision(rb) {
	var collision = new HEFESTO.Collision(HEFESTO.CollisionType.SPHERE_AND_TRUEPLANE, cdGround, rb, null);
	simulation.bindCollision(collision);
}

function bindCollisionWithAll(cd, rb) {
	var collision2 = new HEFESTO.Collision(HEFESTO.CollisionType.ALL, cd, rb, null);
	simulation.bindCollision(collision2);
}

function anima() {
	if (!simulation.isBusy()) {
		requestAnimationFrame(anima);
		controls.update();
		timming.update();
		render.render(scene, camera);
	}
}